package ru.suyundukov.tm.service;

import org.apache.ibatis.session.SqlSession;
import ru.suyundukov.tm.MyBatisConfig;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.repository.ProjectRepository;

import java.sql.SQLException;
import java.util.List;

public class ProjectService implements ProjectServiceInterface {

    public void updateProject(Project project) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                projectRepository.updateProject(project);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public void save(Project project) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                projectRepository.save(project);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public Project findOne(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                return projectRepository.findOne(id);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public Project findByName(String name) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                return projectRepository.findByName(name);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public List<Project> findAll() throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                return projectRepository.findAll();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    public void delete(String name) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                projectRepository.delete(name);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public void delete(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final ProjectRepository projectRepository = sqlSession.getMapper(ProjectRepository.class);
            try {
                projectRepository.deleteById(id);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Project[] projects) throws Exception {
        for (Project project : projects) {
            save(project);
        }
    }

    @Override
    public void load(List<Project> projectList) throws Exception {
        for (Project project : projectList) {
            save(project);
        }
    }
}
