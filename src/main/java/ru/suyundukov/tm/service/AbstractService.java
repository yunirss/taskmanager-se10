package ru.suyundukov.tm.service;

import java.sql.SQLException;
import java.util.List;

public interface AbstractService<E> {
    void save(E entity) throws Exception;

    E findOne(Integer id) throws Exception;

    E findByName(String name) throws Exception;

    List<E> findAll() throws Exception;

    void delete(Integer id) throws Exception;

    boolean exist(Integer id) throws Exception;

    void load(E[] es) throws Exception;

    void load(List<E> es) throws Exception;
}
