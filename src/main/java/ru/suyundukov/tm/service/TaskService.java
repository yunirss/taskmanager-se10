package ru.suyundukov.tm.service;

import org.apache.ibatis.session.SqlSession;
import ru.suyundukov.tm.MyBatisConfig;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.repository.TaskRepository;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TaskService implements TaskServiceInterface {
    @Override
    public void save(Task task) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                taskRepository.save(task);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public void delete(String name) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                taskRepository.delete(name);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    public void deleteTasksByProjectId(int id) throws Exception {
        for (Task task : findTasksByProjectId(id)) {
            delete(task.getId());
        }
    }

    public List<Task> findTasksByProjectId(int projectId) throws IOException {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                return taskRepository.findTasksByProjectId(projectId);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public List<Task> findTasksByUserId(int userId) throws IOException {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                return taskRepository.findTasksByUserId(userId);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    public void updateTask(Task task) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                taskRepository.updateTask(task);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public Task findOne(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                return taskRepository.findOne(id);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public Task findByName(String name) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
               return taskRepository.findByName(name);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public List<Task> findAll() throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                return taskRepository.findAll();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public void delete(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final TaskRepository taskRepository = sqlSession.getMapper(TaskRepository.class);
            try {
                taskRepository.deleteById(id);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(Task[] tasks) throws Exception {
        for (Task task : tasks) {
            save(task);
        }
    }

    @Override
    public void load(List<Task> tasks) throws Exception {
        for (Task task : tasks) {
            save(task);
        }
    }
}