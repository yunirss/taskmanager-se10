package ru.suyundukov.tm.service;

import org.apache.ibatis.session.SqlSession;
import ru.suyundukov.tm.MyBatisConfig;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.repository.UserRepository;

import java.sql.SQLException;
import java.util.List;

public class UserService implements UserServiceInterface {
    private User userOnline;
    public User getUserOnline() {
        return userOnline;
    }

    public void setUserOnline(User userOnline) {
        this.userOnline = userOnline;
    }

    @Override
    public void save(User user) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            try {
                userRepository.save(user);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public User findOne(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            try {
                return userRepository.findOne(id);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public User findByName(String name) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            try {
                return userRepository.findByName(name);
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public List<User> findAll() throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            try {
                return userRepository.findAll();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
        return null;
    }

    @Override
    public void delete(Integer id) throws Exception {
        try (final SqlSession sqlSession = MyBatisConfig.getSqlSessionFactory().openSession()) {
            final UserRepository userRepository = sqlSession.getMapper(UserRepository.class);
            try {
                userRepository.delete(id);
                sqlSession.commit();
            } catch (SQLException e) {
                e.printStackTrace();
                sqlSession.rollback();
            }
        }
    }

    @Override
    public boolean exist(Integer id) throws Exception {
        return findAll().isEmpty();
    }

    @Override
    public void load(User[] users) throws Exception {
        for (User user : users) {
            save(user);
        }
    }

    @Override
    public void load(List<User> users) throws Exception {
        for (User user : users) {
            save(user);
        }
    }
}