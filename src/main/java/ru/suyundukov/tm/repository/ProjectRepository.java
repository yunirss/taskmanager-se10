package ru.suyundukov.tm.repository;

import org.apache.ibatis.annotations.*;
import ru.suyundukov.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public interface ProjectRepository {
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "userId", column = "userid"),
            @Result(property = "createDate", column = "createdate"),
            @Result(property = "status", column = "status"),
    })

    @Insert("INSERT INTO \"Project\" (name, description, userid, createdate, status) " +
            "VALUES (#{name}, #{description}, #{userId}, #{createDate}, #{status})")
    void save(Project project) throws SQLException;

    @Select("SELECT * FROM \"Project\"")
    List<Project> findAll() throws SQLException;

    @Select("SELECT * FROM \"Project\" WHERE id=#{id}")
    Project findOne(Integer id) throws SQLException;

    @Select("SELECT * FROM \"Project\" WHERE name=#{name}")
    Project findByName(String name) throws Exception;

    @Select("SELECT * FROM \"Project\" WHERE userid = #{userId}")
    List<Project> findProjectsByUserId(int userId) throws SQLException;

    @Update("UPDATE \"Project\" SET description = #{description}, status = #{status} WHERE id = #{id}")
    void updateProject(Project project) throws Exception;

    @Delete("DELETE FROM \"Project\" WHERE id =#{id}")
    void deleteById(Integer id) throws SQLException;

    @Delete("DELETE FROM \"Project\" WHERE name =#{name}")
    void delete(String name) throws SQLException;
}
