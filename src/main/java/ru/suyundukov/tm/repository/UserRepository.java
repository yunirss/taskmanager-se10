package ru.suyundukov.tm.repository;

import org.apache.ibatis.annotations.*;
import ru.suyundukov.tm.entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserRepository {
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "login", column = "login"),
            @Result(property = "password", column = "password"),
            @Result(property = "createDate", column = "createdate"),
            @Result(property = "roleType", column = "roletype"),
            @Result(property = "name", column = "name"),
    })

    @Insert("INSERT INTO public.\"User\" (login, password, createdate, roletype, name) VALUES (#{login}, #{password}, #{createDate}, #{roleType}, #{name})")
    void save(User user) throws SQLException;

    @Select("SELECT * FROM public.\"User\"")
    List<User> findAll() throws SQLException;

    @Select("SELECT * FROM public.\"User\" WHERE id=#{id}")
    User findOne(Integer id) throws SQLException;

    @Select("SELECT * FROM public.\"User\" WHERE name=#{name}")
    User findByName(String name) throws SQLException;

    @Delete("DELETE FROM public.\"User\" WHERE id =#{id}")
    void delete(Integer id) throws SQLException;
}
