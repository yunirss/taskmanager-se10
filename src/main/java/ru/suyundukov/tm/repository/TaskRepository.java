package ru.suyundukov.tm.repository;

import org.apache.ibatis.annotations.*;
import ru.suyundukov.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public interface TaskRepository {
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "createDate", column = "createdate"),
            @Result(property = "userId", column = "userid"),
            @Result(property = "projectId", column = "projectid"),
            @Result(property = "status", column = "status"),
    })

    @Insert("INSERT INTO \"Task\" (name, description, createdate, userid, projectid, status) " +
            "VALUES(#{name},#{description}, #{createDate}, #{userId}, #{projectId}, #{status})")
    void save(Task task) throws SQLException;

    @Select("SELECT * FROM \"Task\"")
    List<Task> findAll() throws SQLException;

    @Select("SELECT * FROM \"Task\" WHERE id = #{id}")
    Task findOne(Integer id) throws SQLException;

    @Select("SELECT * FROM \"Task\" WHERE name = #{name}")
    Task findByName(String name) throws SQLException;

    @Delete("DELETE FROM \"Task\" WHERE id = #{id}")
    void deleteById(Integer id) throws SQLException;

    @Select("DELETE FROM \"Task\" WHERE name = #{name}")
    void delete(String name) throws SQLException;

    @Update("UPDATE \"Task\" SET description = #{description}, STATUS = #{status} WHERE ID = #{id}")
    void updateTask(Task task) throws SQLException;

    @Select("SELECT * FROM \"Task\" WHERE projectid = #{projectId}")
    List<Task> findTasksByProjectId(int projectId) throws SQLException;

    @Select("SELECT * FROM \"Task\" WHERE userid = #{userId}")
    List<Task> findTasksByUserId(int userId) throws SQLException;
}
