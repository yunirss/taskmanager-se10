package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.suyundukov.tm.enums.Status;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Класс, для работы с сущностью проекта
 */
@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
public class Task extends AbstractEntity implements Serializable {
    private String description;
    private int projectId;
    private int userId;
    private Status status;

    public Task(Integer id, String name, String description, LocalDateTime createDate, int userId, int projectId, Status status) {
        super(id, name, createDate);
        this.description = description;
        this.projectId = projectId;
        this.userId = userId;
        this.status = status;
    }
}
