package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.suyundukov.tm.enums.RoleType;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User extends AbstractEntity implements Serializable {
    private String login;
    private String password;
    private RoleType roleType;

    public User(Integer id, String name, String login, String password, LocalDateTime createDate, RoleType roleType) {
        super(id, name, createDate);
        this.login = login;
        this.password = password;
        this.roleType = roleType;
    }
}