package ru.suyundukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@Setter
public abstract class AbstractEntity implements Serializable {
    protected Integer id;
    protected String name;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.")
    protected LocalDateTime createDate;

    protected AbstractEntity(Integer id, String name, LocalDateTime createDate) {
        this.id = id;
        this.name = name;
        this.createDate = LocalDateTime.now();
    }

    public AbstractEntity() {
    }
}
