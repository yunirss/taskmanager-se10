package ru.suyundukov.tm;

import org.reflections.Reflections;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.service.ProjectService;
import ru.suyundukov.tm.service.TaskService;
import ru.suyundukov.tm.service.UserService;
import ru.suyundukov.tm.utils.ConsoleHelper;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class Bootstrap implements ServiceLocator {
    private ProjectService projectServiceImpl;
    private TaskService taskServiceImpl;
    private UserService userService;
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Set<Class<? extends AbstractCommand>> commandClasses = getAbstractCommandClasses();

    private Set<Class<? extends AbstractCommand>> getAbstractCommandClasses() {
        return new Reflections("ru.suyundukov.tm").getSubTypesOf(AbstractCommand.class);
    }

    public void init() throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        for (final Class<? extends AbstractCommand> abstractClass : commandClasses) {
            registry(abstractClass.getDeclaredConstructor().newInstance());
        }
    }

    private void registry(final AbstractCommand command) {
        final String commandName = command.getName();
        command.setServiceLocator(this);
        commands.put(commandName, command);
    }

    public void start() {
        try {
            projectServiceImpl = new ProjectService();
            taskServiceImpl = new TaskService();
            userService = new UserService();
            init();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        System.out.println("WELCOME TO TASK MANAGER");
        while (true) {
            try {
                String string = ConsoleHelper.readString().trim();
                AbstractCommand command = null;
                for (AbstractCommand abstractCommand : getCommands()) {
                    if (abstractCommand.getName().equalsIgnoreCase(string)) {
                        if (userService.getUserOnline() == null) {
                            if (abstractCommand.isNeedAuthorization()) {
                                System.out.println("[YOU NEED LOG IN TO USE THIS COMMANDS]");
                                break;
                            }
                        }
                        command = abstractCommand;
                        command.setServiceLocator(this);
                        command.execute();
                        break;
                    }
                }
                if (command == null) {
                    System.out.println("[PLEASE, TYPE CORRECT COMMAND OR ENTER <<help>>]");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @Override
    public ProjectService getProjectService() {
        return projectServiceImpl;
    }

    @Override
    public TaskService getTaskService() {
        return taskServiceImpl;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }
}