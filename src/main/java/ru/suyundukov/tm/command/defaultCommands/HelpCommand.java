package ru.suyundukov.tm.command.defaultCommands;

import ru.suyundukov.tm.command.AbstractCommand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс, для работы с сущностью проекта, содержащий дефолтные команды
 */
public class HelpCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        List<String> listOfCommands = new ArrayList<>();
        if (serviceLocator.getUserService().getUserOnline() == null) {
            for (AbstractCommand command : serviceLocator.getCommands()) {
                if (!command.isNeedAuthorization()) {
                    listOfCommands.add(command.getName() + " : " + command.getDescription());
                }
            }
        } else {
            for (AbstractCommand command : serviceLocator.getCommands()) {
                listOfCommands.add(command.getName() + " : " + command.getDescription());
            }
        }
        Collections.sort(listOfCommands);
        for (String s : listOfCommands) {
            System.out.println(s);
        }
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return false;
    }
}
