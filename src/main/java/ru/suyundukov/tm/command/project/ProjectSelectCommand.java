package ru.suyundukov.tm.command.project;

import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;

import java.util.Comparator;
import java.util.List;

public class ProjectSelectCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER PROJECT NAME] :");
        String projectName = ConsoleHelper.readString();
        final Project project = serviceLocator.getProjectService().findByName(projectName);
        User userOnline = serviceLocator.getUserService().getUserOnline();
        if (project.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
            System.out.println("[PROJECT ID] - " + project.getId());
            System.out.println("[PROJECT NAME] - " + project.getName());
            System.out.println("[PROJECT DESCRIPTION] - " + project.getDescription());
            System.out.println("[PROJECT STATUS] - " + project.getStatus().getName());
            System.out.println("[CREATION DATE] - " + DateHelper.dateFormat(project.getCreateDate()));
            System.out.println("[CREATED BY] - " + serviceLocator.getUserService().findOne(project.getUserId()).getName());
            List<Task> taskToShow = serviceLocator.getTaskService().findTasksByProjectId(project.getId());
            if (taskToShow.isEmpty()) {
                System.out.println(("[PROJECT TASKS] : EMPTY"));
            } else {
                System.out.println(("[PROJECT TASKS] : "));
                taskToShow.sort(Comparator.comparing(Task::getName));
                for (int i = 0; i < taskToShow.size(); i++) {
                    System.out.println((i + 1) + ". " + taskToShow.get(i).getName());
                }
            }
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    @Override
    public String getName() {
        return "project-select";
    }

    @Override
    public String getDescription() {
        return "Select project";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
