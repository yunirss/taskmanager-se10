package ru.suyundukov.tm.command.project;

import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.enums.Status;
import ru.suyundukov.tm.utils.ConsoleHelper;

public class ProjectEditCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT EDIT]");
        System.out.println("[ENTER NAME OF PROJECT TO EDIT]");
        String projectName = ConsoleHelper.readString();
        final User userOnline = serviceLocator.getUserService().getUserOnline();
        final Project project = serviceLocator.getProjectService().findByName(projectName);
        if ((project.getUserId() == userOnline.getId()) || userOnline.getRoleType().equals(RoleType.ADMIN)) {
            if (project.getName() != null) {
                System.out.println("[ENTER NEW DESCRIPTION]");
                project.setDescription(ConsoleHelper.readString());
                System.out.println("[NEW DESCRIPTION ADDED]");
                System.out.println("[ENTER NEW STATUS: open, inprogress OR resolved]");
                String status = ConsoleHelper.readString().trim();
                while (true) {
                    if (status.equalsIgnoreCase(Status.OPEN.getName())) {
                        project.setStatus(Status.OPEN);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else if (status.equalsIgnoreCase(Status.INPROGRESS.getName())) {
                        project.setStatus(Status.INPROGRESS);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else if (status.equalsIgnoreCase(Status.RESOLVED.getName())) {
                        project.setStatus(Status.RESOLVED);
                        System.out.println("[NEW STATUS ADDED]");
                        break;
                    } else {
                        System.out.println("[WRONG COMMAND]");
                    }
                }
                serviceLocator.getProjectService().updateProject(project);
            } else {
                System.out.println("[NO SUCH PROJECT EXIST]");
            }
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    @Override
    public String getName() {
        return "project-edit";
    }

    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
