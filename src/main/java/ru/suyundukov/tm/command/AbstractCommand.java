package ru.suyundukov.tm.command;

import ru.suyundukov.tm.ServiceLocator;

public abstract class AbstractCommand {
    public ServiceLocator serviceLocator;

    public abstract void execute() throws Exception;

    public abstract String getName();

    public abstract String getDescription();

    public abstract boolean isNeedAuthorization();

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }
}
