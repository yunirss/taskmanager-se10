package ru.suyundukov.tm.command.user;

import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;

import java.time.LocalDateTime;

public class UserSignUpCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[USER CREATED]");
        System.out.println("[ENTER NAME:]");
        String name = ConsoleHelper.readString();
        System.out.println("[ENTER LOGIN:]");
        String login = ConsoleHelper.readString();
        System.out.println("[ENTER PASSWORD:]");
        String password = ConsoleHelper.readString();
        String passwordMD5 = ConsoleHelper.md5Password(password);
        RoleType roleType;
        System.out.println("[ENTER ROLL-TYPE : admin or user]");
        while (true) {
            String roll = ConsoleHelper.readString();
            if (roll.equalsIgnoreCase("admin")) {
                roleType = RoleType.ADMIN;
                User newUser = new User(null, name, login, passwordMD5, LocalDateTime.now(), roleType);
                serviceLocator.getUserService().save(newUser);
                System.out.println("[NEW ADMIN CREATED]");
                break;
            } else if (roll.equalsIgnoreCase("user")) {
                roleType = RoleType.USER;
                User newUser = new User(null, name, login, passwordMD5, LocalDateTime.now(), roleType);
                serviceLocator.getUserService().save(newUser);
                System.out.println("[NEW USER CREATED]");
                break;
            } else {
                System.out.println("[WRONG COMMAND, TRY AGAIN]");
            }
        }
    }

    @Override
    public String getName() {
        return "user-signup";
    }

    @Override
    public String getDescription() {
        return "New user registration";
    }

    @Override
    public boolean isNeedAuthorization() {
        return false;
    }
}
