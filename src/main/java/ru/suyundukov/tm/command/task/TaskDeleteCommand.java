package ru.suyundukov.tm.command.task;

import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;

public class TaskDeleteCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]");
        System.out.println("[ENTER TASK NAME]");
        String taskName = ConsoleHelper.readString();
        final Task task = serviceLocator.getTaskService().findByName(taskName);
        if (task.getId() != null) {
            User userOnline = serviceLocator.getUserService().getUserOnline();
            if (task.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
                System.out.println("[DELETE TASK " + "<<" + taskName + ">>" + " ? YES or NO]");
                String name = ConsoleHelper.readString();
                if (name.equalsIgnoreCase("yes")) {
                    serviceLocator.getTaskService().delete(task.getId());
                    System.out.println("[SELECTED TASK DELETED]");
                } else {
                    System.out.println("[NOT DELETED]");
                }
                return;
            }
            System.out.println("[ACCESS DENIED]");
        } else {
            System.out.println("[NO SUCH PROJECT EXIST]");
        }
    }

    @Override
    public String getName() {
        return "task-delete";
    }

    @Override
    public String getDescription() {
        return "Delete selected task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
