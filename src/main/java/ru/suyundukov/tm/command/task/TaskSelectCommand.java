package ru.suyundukov.tm.command.task;

import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.enums.RoleType;
import ru.suyundukov.tm.utils.ConsoleHelper;
import ru.suyundukov.tm.utils.DateHelper;

public class TaskSelectCommand extends AbstractCommand {
    @Override
    public void execute() throws Exception {
        System.out.println("[ENTER TASK NAME] : ");
        String taskName = ConsoleHelper.readString();
        final Task task = serviceLocator.getTaskService().findByName(taskName);
        final User userOnline = serviceLocator.getUserService().getUserOnline();
        if (task.getUserId() == userOnline.getId() || userOnline.getRoleType().equals(RoleType.ADMIN)) {
            System.out.println("[TASK ID] - " + task.getId());
            System.out.println("[TASK NAME] - " + task.getName());
            System.out.println("[TASK DESCRIPTION] - " + task.getDescription());
            System.out.println("[TASK STATUS] - " + task.getStatus().getName());
            System.out.println("[PROJECT NAME] - " + serviceLocator.getProjectService().findOne(task.getProjectId()).getName());
            System.out.println("[CREATION DATE] - " + DateHelper.dateFormat(task.getCreateDate()));
            System.out.println("[CREATED BY] - " + serviceLocator.getUserService().findOne(task.getUserId()).getName());
            return;
        }
        System.out.println("[ACCESS DENIED]");
    }

    @Override
    public String getName() {
        return "task-select";
    }

    @Override
    public String getDescription() {
        return "Select task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
