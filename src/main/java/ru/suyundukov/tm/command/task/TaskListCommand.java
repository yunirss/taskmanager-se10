package ru.suyundukov.tm.command.task;

import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;
import ru.suyundukov.tm.command.AbstractCommand;
import ru.suyundukov.tm.enums.RoleType;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskListCommand extends AbstractCommand {

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        List<Task> taskList = serviceLocator.getTaskService().findAll();
        final User userOnline = serviceLocator.getUserService().getUserOnline();
        if (!userOnline.getRoleType().equals(RoleType.ADMIN)) {
            List<Task> taskValid = taskList.stream()
                    .filter(x -> userOnline.getId() == x.getUserId())
                    .sorted(Comparator.comparing(Task::getName))
                    .collect(Collectors.toList());
            for (int i = 0; i < taskValid.size(); i++) {
                System.out.println((i + 1) + ". " + taskValid.get(i).getName());
            }
        } else {
            taskList.sort(Comparator.comparing(Task::getName));
            for (int i = 0; i < taskList.size(); i++) {
                System.out.println((i + 1) + ". " + taskList.get(i).getName());
            }
        }
    }


    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all task.";
    }

    @Override
    public boolean isNeedAuthorization() {
        return true;
    }
}
