package ru.suyundukov.tm;

import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import ru.suyundukov.tm.repository.*;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Properties;

public class MyBatisConfig {
    public static SqlSessionFactory getSqlSessionFactory() throws IOException {
        Properties properties = new Properties();
        properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("database.properties"));
        final String user = properties.getProperty("db.login");
        final String password = properties.getProperty("db.password");
        final String url = properties.getProperty("db.host");
        final String driver = properties.getProperty("db.driver");
        final DataSource dataSource = new PooledDataSource(driver, url, user, password);
        final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        final Environment environment = new Environment("development", transactionFactory, dataSource);
        final Configuration configuration = new Configuration(environment);
        configuration.addMapper(TaskRepository.class);
        configuration.addMapper(ProjectRepository.class);
        configuration.addMapper(UserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }
}