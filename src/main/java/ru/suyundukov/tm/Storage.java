package ru.suyundukov.tm;

import ru.suyundukov.tm.entity.Project;
import ru.suyundukov.tm.entity.Task;
import ru.suyundukov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Класс, содержащий списки задач и проектов
 */
public class Storage {
    private final List<Project> projectList;
    private final List<Task> taskList;
    private final List<User> userList;
    private static volatile Storage instance;

    private Storage() {
        this.projectList = new ArrayList<>();
        this.taskList = new ArrayList<>();
        this.userList = new ArrayList<>();
    }

    public static Storage getInstance() {
        Storage localInstance = instance;
        if (localInstance == null) {
            synchronized (Storage.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Storage();
                }
            }
        }
        return localInstance;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public List<User> getUserList() {
        return userList;
    }
}
